<?php

include 'app/models/User.php';

class UserController extends Controller{
    function render(){

        $template=new Template;
        echo $template->render('login.htm');
        // echo "Login";
    }

    function beforeroute(){
    }

    function authenticate($f3) {

        $username = $this->f3->get('POST.username');
        $password = $this->f3->get('POST.password');

        $user = new User($this->db);
        $user->getByName($username);

        if($user->dry()) {
            echo "dry";
            // $this->f3->reroute($this->$f3->get('BASE') . '/admin/login');
        }

        if(password_verify($password, $user->password)) {
            
            $f3->set('SESSION.user', $user->email);
            $route = $f3->get("BASE") . "/admin";
            $f3->reroute($route );
        } else {
            $route = $f3->get("BASE") . "/admin";
            $f3->reroute($route);
        }
    }

    function authenticateTest($f3) {

        $username = $this->f3->get('POST.username');
        $password = $this->f3->get('POST.password');

        echo $username . " : " . $password;
    }
 
    function logout($f3){
        $f3->set('SESSION.user', null);
        $route = $f3->get("BASE") . "/admin";
            $f3->reroute($route );
    }
    
    function displayProfile($f3){
        
        

        $db = $this->db;
        $user = $f3->get('SESSION.user');

        $user = $db->exec("SELECT * FROM `users` WHERE `email` = '" . $user . "'");

        $sql = "SELECT `users`.`officer_id`,`officer`.`first_name`,`officer`.`last_name`,`users`.`email`, `entity`.`authorized_officer`, `entity`.`business_contact`,`entity`.`president`,`entity`.`vice_president`, `officer`.`phone`, `entity`.`entity_name`";
        $sql .= "FROM `users` ";
        $sql .= "JOIN `officer` ON `users`.`officer_id` = `officer`.`officer_id` ";
        $sql .= "JOIN `entity` ON (`users`.`officer_id` = `entity`.`authorized_officer` OR ";
        $sql .= "`users`.`officer_id` = `entity`.`business_contact` OR ";
        $sql .= "`users`.`officer_id` = `entity`.`president` OR ";
        $sql .= "`users`.`officer_id` = `entity`.`vice_president`)  ";
        $sql .= "WHERE `users`.`email` = '" . $user[0]['email'] . "'  ";
        $officerQuery = $db->exec($sql);

        // echo "<pre>";
        // print_r($officerQuery);
        // exit;
        $officerId = $officerQuery[0]['officer_id'];
        // $roleQuery = $db->exec("SELECT * FROM `officer` WHERE `authorized_officer` = '" . $officerId . "'");
        $officer = $officerQuery[0];

        $positions = [];
        foreach($officerQuery as $row){
            $id = $user[0]['officer_id'];
            if($id == $row['authorized_officer']){
                $positions[] = array('title' => 'Authorized Officer', 'entity' => $row['entity_name'] );
            }
            if($id == $row['business_contact']){
                $positions[] = array('title' => 'Business Contact', 'entity' => $row['entity_name'] );
            }
            if($id == $row['president']){
                $positions[] = array('title' => 'President', 'entity' => $row['entity_name'] );
            }
            if($id == $row['vice_president']){
                $positions[] = array('title' => 'Vice-President', 'entity' => $row['entity_name'] );
            }
        }

        if(count($positions) == 0){
            $positions[] = array('title' => 'No official titles held', 'entity' => 'any entities');
        }
        if($officer['officer_id'] == "1001"){
            $officer['officer_id'] = "n/a";
        }

        // echo "<pre>";
        // print_r($officerQuery);
        // print_r($positions);
        // exit;
        

        // echo "<pre>";
        // print_r($user[0]);
        // print_r($officerId);
        $f3->set('user', $user[0]);
        $f3->set('officer', $officer);
        $f3->set('positions', $positions);
        $f3->set('content','user-profile.htm');
		echo Template::instance()->render('layout.htm');
    }
}
