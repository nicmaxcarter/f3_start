<?php

require 'vendor/autoload.php';

$f3 = \Base::instance();

$f3->config('app/config.ini');
$f3->config('app/routes.ini');

$f3->set('ONERROR',function($f3){
    echo \Template::instance()->render('frontend/404.htm');
  });


// use the method below or extract routes to routes.ini
// $f3->route('GET /',
//     function() {
//         echo 'Hello, world!';
//     }
// );

$f3->run();